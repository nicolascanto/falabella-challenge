//
//  Extensions.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/2/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionButton = UIAlertAction(title: "Aceptar", style: .default)
        alert.addAction(actionButton)
        self.present(alert, animated: true)
    }
}
