//
//  User.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/4/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation

class User {
    private static var user: UserLoginEntity?
    static func currentUser() -> UserLoginEntity? {
        return User.user
    }
    static func setUser(_ user: UserLoginEntity) {
        User.user = user
    }
}
