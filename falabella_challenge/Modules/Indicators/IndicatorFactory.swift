//
//  IndicatorFactory.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/2/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation
import UIKit

public class IndicatorFactory {
    func getViewController() -> UIViewController {
        let service = IndicatorService()
        let model = IndicatorModel(service)
        let presenter = IndicatorPresenter(model)
        let vc = IndicatorViewController()
        presenter.view = vc
        vc.presenter = presenter
        return vc
    }
}
