//
//  IndicatorPresenter.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/2/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation

protocol IndicatorPresenterProtocol {
    func getIndicators() -> Void
    func setIndicator(_ indicator: IndicatorEntity)
    var dataSource: IndicatorModelDataSourceProtocol { get set }
    
}

class IndicatorPresenter: IndicatorPresenterProtocol {
    
    weak var view: IndicatorViewProtocol?
    let model: IndicatorModelProtocol
    var dataSource: IndicatorModelDataSourceProtocol
    
    init(_ model: IndicatorModel) {
        self.model = model
        self.dataSource = model
    }
    
    
    func getIndicators() {
        self.model.getIndicators { (state) in
            switch state {
            case .OK:
            self.view?.loadOK()
            case .ERROR:
            self.view?.loadERROR()
            case .EMPTY:
            self.view?.loadEMPTY()
            }
        }
    }
    
    func setIndicator(_ indicator: IndicatorEntity) {
        self.model.setIndicator(indicator)
    }
    
    
}
