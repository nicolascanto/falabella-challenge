//
//  IndicatorService.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/2/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation
import Alamofire

protocol IndicatorServiceProtocol {
    func getIndicators(callback: @escaping (IndicatorRequest?) -> Void)
}

class IndicatorService: IndicatorServiceProtocol {
    func getIndicators(callback: @escaping (IndicatorRequest?) -> Void) {        
        AF.request("https://www.mindicador.cl/api").responseData { (req) in
            guard let data = req.data else {
                return callback(nil)
            }
            do {
                callback(try JSONDecoder().decode(IndicatorRequest.self, from: data))
            } catch let e {
                print(e)
                callback(nil)
            }
        }
    }
}
