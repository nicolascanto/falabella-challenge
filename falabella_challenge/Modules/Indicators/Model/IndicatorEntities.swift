//
//  IndicatorEntities.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/2/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation
import UIKit

struct IndicatorRequest: Codable {
    var version: String
    var autor: String
    var fecha: String
    var uf: IndicatorEntity?
    var ivp: IndicatorEntity?
    var dolar: IndicatorEntity?
    var dolar_intercambio: IndicatorEntity?
    var euro: IndicatorEntity?
    var ipc: IndicatorEntity?
    var utm: IndicatorEntity?
    var imacec: IndicatorEntity?
    var tpm: IndicatorEntity?
    var libra_cobre: IndicatorEntity?
    var tasa_desempleo: IndicatorEntity?
    var bitcoin: IndicatorEntity?
    
    
    
}

struct IndicatorEntity: Codable {
    var codigo: String
    var nombre: String
    var unidad_medida: String
    var fecha: String
    var valor: CGFloat
}
