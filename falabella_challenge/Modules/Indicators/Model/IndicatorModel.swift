//
//  IndicatorModel.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/2/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation

protocol IndicatorModelProtocol {
    func getIndicators(callback: @escaping (State) -> Void)
    func setIndicator(_ indicator: IndicatorEntity)
}

protocol IndicatorModelDataSourceProtocol {
    var indicators: [IndicatorEntity] {get set}
    var selectedIndicator: IndicatorEntity? { get set }
}

class IndicatorModel: IndicatorModelProtocol, IndicatorModelDataSourceProtocol {
  
    var indicators: [IndicatorEntity] = []
    var selectedIndicator: IndicatorEntity?
    private let service: IndicatorService
    
    init(_ service: IndicatorService) {
        self.service = service
    }
    
    func getIndicators(callback: @escaping (State) -> Void) {
        self.service.getIndicators { (response) in
            guard let res = response else {
               return callback(.ERROR)
            }
            
            if let uf = res.uf {
                self.indicators.append(uf)
            }
            
            if let ivp = res.ivp {
                self.indicators.append(ivp)
            }
            
            if let dolar = res.dolar {
                self.indicators.append(dolar)
            }
            
            if let dolar_intercambio = res.dolar_intercambio {
                self.indicators.append(dolar_intercambio)
            }
            
            if let euro = res.euro {
                self.indicators.append(euro)
            }
            
            if let ipc = res.ipc {
                self.indicators.append(ipc)
            }
            
            if let utm = res.utm {
                self.indicators.append(utm)
            }
            
            if let imacec = res.imacec {
                self.indicators.append(imacec)
            }
            
            if let tpm = res.tpm {
                self.indicators.append(tpm)
            }
            
            if let imacec = res.imacec {
                self.indicators.append(imacec)
            }
            
            if let libra_cobre = res.libra_cobre {
                self.indicators.append(libra_cobre)
            }
            
            if let tasa_desempleo = res.tasa_desempleo {
                self.indicators.append(tasa_desempleo)
            }
            
            if let bitcoin = res.bitcoin {
                self.indicators.append(bitcoin)
            }
            
            if self.indicators.isEmpty {
                return callback(.EMPTY)
            }
            
            return callback(.OK)
        
        }
    }
    
    func setIndicator(_ indicator: IndicatorEntity) {
        self.selectedIndicator = indicator
    }
}
