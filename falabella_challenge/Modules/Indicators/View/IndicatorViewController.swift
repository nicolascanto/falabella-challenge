//
//  IndicatorViewController.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/2/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation
import UIKit

protocol IndicatorViewProtocol: class {
    func loadOK()
    func loadERROR()
    func loadEMPTY()
}

class IndicatorViewController: UIViewController {
  
    @IBOutlet weak var tableView: UITableView!
    var presenter: IndicatorPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.getIndicators()
    }
    
    func setupView() {
        var title: String? = "Hola "
        if let user = User.currentUser() {
            title! += user.fullName()
        }

        navigationItem.title = title
//        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Volver", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.prefersLargeTitles = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "IndicatorTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    }
    
}

extension IndicatorViewController: IndicatorViewProtocol {
    func loadEMPTY() {
      
    }

    func loadOK() {
        self.setupView()
    }

    func loadERROR() {
      
    }
}

extension IndicatorViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let presenter = self.presenter else {
            return 0
        }
        return presenter.dataSource.indicators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let presenter = self.presenter else {
            fatalError("No se pudo cargar el presenter")
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? IndicatorTableViewCell else {
            fatalError("No se pudo cargar el presenter")
        }
        
        cell.indicator = presenter.dataSource.indicators[indexPath.item]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let presenter = self.presenter else {
            return
        }
        let indicator = presenter.dataSource.indicators[indexPath.row]
        let vc = IndicatorDetailsViewController()
        vc.selectedIndicator = indicator
        navigationController?.pushViewController(vc, animated: true)
    }
}
