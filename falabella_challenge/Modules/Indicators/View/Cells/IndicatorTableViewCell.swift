//
//  IndicatorTableViewCell.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/3/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation
import UIKit

class IndicatorTableViewCell: UITableViewCell {
    
    @IBOutlet weak var indicatorName: UILabel!
    @IBOutlet weak var indicatorValue: UILabel!
    
    
    var indicator: IndicatorEntity? {
        didSet {
            guard let indicator = indicator else  {
                return
            }
            self.indicatorName.text = indicator.nombre
            self.indicatorValue.text = "\(indicator.valor)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
