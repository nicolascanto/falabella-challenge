//
//  IndicatorDetailsViewController.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/3/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation

import UIKit

class IndicatorDetailsViewController: UIViewController {
   
    var selectedIndicator: IndicatorEntity!
    
    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var measurementUnit: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var value: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    func setupView() {
        navigationItem.title = "Detalle"
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.prefersLargeTitles = true
        self.code.text = selectedIndicator.codigo
        self.name.text = selectedIndicator.nombre
        self.measurementUnit.text = selectedIndicator.unidad_medida
        self.date.text = selectedIndicator.fecha
        self.value.text = "\(selectedIndicator.valor)"
    }
    
}
