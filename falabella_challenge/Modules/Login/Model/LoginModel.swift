//
//  LoginModel.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/1/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation
import UIKit

enum State {
    case OK
    case ERROR
    case EMPTY
}

protocol LoginModelProtocol {
    func authentication(username: String, password: String, _ callback: @escaping (State) -> Void)
}

protocol LoginModelDataSourceProtocol {
    var user: UserLoginEntity? {get set}
}

class LoginModel: LoginModelProtocol, LoginModelDataSourceProtocol {
    var user: UserLoginEntity?
    private let service: LoginService
    
    init(_ service: LoginService) {
        self.service = service
    }
    
    func authentication(username: String, password: String, _ callback: @escaping (State) -> Void) {        
        service.authentication(username: username, password: password) { [weak self] response in
            guard let res = response, let self = self else {
               return callback(.ERROR)
            }
            if res.status == 200 {
                self.user = res.payload
                User.setUser(res.payload) 
                return callback(.OK)
            } else {
                return callback(.ERROR)
            }
        }
    }
    
    
}
