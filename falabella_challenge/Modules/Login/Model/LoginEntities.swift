//
//  LoginEntities.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/1/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation

struct IRequest: Codable {
    let status: Int
    let payload: UserLoginEntity
}

struct UserLoginEntity: Codable {
    let username: String
    let firstName: String
    let lastName: String
    
    func fullName() -> String {
        return "\(self.firstName) \(self.lastName)"
    }
}
