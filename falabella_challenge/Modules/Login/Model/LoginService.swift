//
//  LoginService.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/1/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation
import Alamofire

protocol LoginServiceProtocol {
    func authentication(username: String, password: String, callback: @escaping (IRequest?) -> Void)
}

class LoginService: LoginServiceProtocol {
    func authentication(username: String, password: String, callback: @escaping (IRequest?) -> Void) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            let crypto = CriptoUtils()
            guard let username_crypted = crypto.encript(str: username),
                let password_crypted = crypto.encript(str: password), let usernameStored = UserDefaults.standard.string(forKey: "username"),
            let passwordStored = UserDefaults.standard.string(forKey: "password"),
            let nameStored = UserDefaults.standard.string(forKey: "name"),
            let lastnameStored = UserDefaults.standard.string(forKey: "lastName") else {
                return callback(nil)
            }
            
            if username_crypted == usernameStored && password_crypted == passwordStored {
                return callback(IRequest(status: 200, payload: UserLoginEntity(username: username, firstName: nameStored, lastName: lastnameStored)))
            } else {
                callback(nil)
            }
        })
    }
    
    
}
