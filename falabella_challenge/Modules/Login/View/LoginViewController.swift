//
//  LoginViewController.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/1/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation
import UIKit

protocol LoginViewProtocol: class {
    func loadOK()
    func loadERROR()
}

class LoginViewController: UIViewController {
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var AI: UIActivityIndicatorView!
    @IBOutlet weak var tapButton: UIButton!
    
    var presenter: LoginPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AI.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tapButton.isEnabled = true
    }
    
    @IBAction func tapLogin(_ sender: Any) {
        print("tapLogin")
        
        let userName = self.userName.text ?? ""
        let password = self.password.text ?? ""
        
        if userName.isEmpty || password.isEmpty {
            self.showAlert(title: "Oops", message: "Debes completar los campos")
            return
        }
        
        self.showLoading()
        self.presenter.callService(username: userName, password: password)
    }
    
    func showLoading() {
        self.AI.isHidden = false
        self.tapButton.isEnabled = false
    }
    
    func hideLoading() {
        self.AI.isHidden = true
        self.tapButton.isEnabled = true
    }
    
}

extension LoginViewController: LoginViewProtocol {
    func loadOK() {
        self.hideLoading()
        guard let payload = self.presenter.dataSource.user else {
            return
        }
        print("payload: \(payload)")
        self.navigationController?.pushViewController(IndicatorFactory().getViewController(), animated: true)
        let fullName = payload.fullName()
        self.navigationController?.navigationItem.title = fullName
        
    }
    func loadERROR() {
        self.hideLoading()
        self.showAlert(title: "Oops", message: "Los datos ingresados son incorrectos")
    }
}
