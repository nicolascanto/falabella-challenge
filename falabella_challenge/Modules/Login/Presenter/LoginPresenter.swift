//
//  LoginPresenter.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/1/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation

protocol LoginPresenterProtocol {
    func callService(username: String, password: String) -> Void
    var dataSource: LoginModelDataSourceProtocol { get set }
}

class LoginPresenter: LoginPresenterProtocol {
    
    weak var view: LoginViewProtocol?
    let model: LoginModelProtocol
    var dataSource: LoginModelDataSourceProtocol
    
    init(_ model: LoginModel) {
        self.model = model
        self.dataSource = model
    } 
    
    func callService(username: String, password: String) {
        model.authentication(username: username, password: password, { [weak self] state in
            guard let self = self, let view = self.view else {
                return
            }
            
            switch state {
            case .OK:
                print("Se cargo la data")
                view.loadOK()
            case .ERROR:
                print("ocurrio un error en el Request")
                view.loadERROR()
            case .EMPTY:
                view.loadERROR()
            }
        })
        
    }
}
