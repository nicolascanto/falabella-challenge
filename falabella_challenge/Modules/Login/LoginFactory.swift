//
//  LoginFactory.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/2/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation
import UIKit

public class LoginFactory {
    func getViewController() -> UIViewController {
        let service = LoginService()
        let model = LoginModel(service)
        let presenter = LoginPresenter(model)
        let vc = LoginViewController()
        presenter.view = vc
        vc.presenter = presenter
        return vc
    }
}
