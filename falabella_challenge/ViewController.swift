//
//  ViewController.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/1/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tapButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    @IBAction func tap(_ sender: Any) {
        print("tap")
        let vc = IndicatorFactory().getViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

