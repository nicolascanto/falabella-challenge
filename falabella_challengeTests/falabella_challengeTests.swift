//
//  falabella_challengeTests.swift
//  falabella_challengeTests
//
//  Created by Nicolás Cantó on 3/1/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import XCTest
@testable import falabella_challenge

class falabella_challengeTests: XCTestCase {
    
    let indicatorModel: IndicatorModel = {
        let service = IndicatorService()
        let model = IndicatorModel(service)
        return model
    }()
    
    let loginModel: LoginModel = {
        let service = LoginService()
        let model = LoginModel(service)
        return model
    }()

    func test_getIndicators_OK() {
        IndicatorsServiceStubs().get_200()
        let expectation = self.expectation(description: "get Collection list")
        indicatorModel.getIndicators{ state in
            XCTAssert(state == .OK)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 1, handler: nil)
        XCTAssert(indicatorModel.indicators.count > 0)
    }
    
    func test_getIndicators_ERROR() {
        IndicatorsServiceStubs().get_500_ERROR()
        let expectation = self.expectation(description: "get Collection list")
        indicatorModel.getIndicators{ state in
            XCTAssert(state == .ERROR)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 1, handler: nil)
        XCTAssert(indicatorModel.indicators.count == 0)
    }
    
    func test_getIndicators_EMPTY() {
        IndicatorsServiceStubs().get_200_EMPTY()
        let expectation = self.expectation(description: "get Collection list")
        indicatorModel.getIndicators{ state in
            XCTAssert(state == .EMPTY)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 1, handler: nil)
        XCTAssert(indicatorModel.indicators.count == 0)
    }
    
    func test_selectedIndicator() {
        let indicator = IndicatorEntity(codigo: "", nombre: "", unidad_medida: "", fecha: "", valor: 0.0)
        
        XCTAssert(indicatorModel.selectedIndicator == nil)
        indicatorModel.setIndicator(indicator)
        XCTAssert(indicatorModel.selectedIndicator != nil)
    }
    
    func test_authentication_OK() {
        loginModel.authentication(username: "nicolas", password: "123456") { (state) in
            XCTAssert(state == .OK)
        }
    }
    
    func test_authentication_ERROR() {
        loginModel.authentication(username: "nicolas", password: "123456") { (state) in
            XCTAssert(state == .ERROR)
        }
    }

}

