//
//  FinderServiceStubs.swift
//  falabella_challenge
//
//  Created by Nicolás Cantó on 3/3/20.
//  Copyright © 2020 Nicolás Cantó. All rights reserved.
//

import Foundation
import OHHTTPStubs

class IndicatorsServiceStubs: NSObject {
    func get_200() {
        let getdFile = "getIndicators_200.json"
        let path = ".*/"
        stub(condition: pathMatches(path)) { _ in
            return fixture(
                filePath: OHPathForFile(getdFile, type(of: self))! ,
                status: 200,
                headers: [:]
            )
        }
    }
    func get_200_EMPTY() {
        let getdFile = "getIndicators_200_EMPTY.json"
        let path = ".*/"
        stub(condition: pathMatches(path)) { _ in
            return fixture(
                filePath: OHPathForFile(getdFile, type(of: self))! ,
                status: 200,
                headers: [:]
            )
        }
    }

    func get_500_ERROR() {
        let getdFile = "getIndicators_500.json"
        let path = ".*/"
        stub(condition: pathMatches(path)) { _ in
            return fixture(
                filePath: OHPathForFile(getdFile, type(of: self))! ,
                status: 500,
                headers: [:]
            )
        }
    }
}
